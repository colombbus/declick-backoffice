import * as types from "../mutation-types";

const state = {
  user:{},
  token:'',
  currentProject:{}
};

const mutations = {
  [types.TOKEN](state, token) {
    state.token = token;
  },
  [types.USER](state, user) {
    state.user = user;
  },
  [types.PROJECT_SELECTION] (state, {project}) {
    state.currentProject = project;
  },
};

export default {
  state,
  mutations
};
