import * as types from './mutation-types'

import Api from "@/api"

export const connection = ({commit, dispatch}, user) => {
    Api.createToken(user.username,user.password).then((r) => {
        localStorage.setItem('token',r)
        commit(types.TOKEN,r)
        dispatch('getUserByToken',r)
        console.log(user);
        
    })
}

export const getUserByToken = ({commit,dispatch},token) => {
    Api.getUserByToken(token).then((r) =>{
        commit(types.USER,r)
        r.token = token;
        dispatch('selectProject',r)        
    })
}

// imported (modified)

// export const selectProject = async ({commit}, user) => {
//   await Api.updateUser(user.id, {
//     currentProjectId: user.currentProjectId
//   }, user.token)
//   let project = await Api.getProject(user.currentProjectId, user.token)
//   commit(types.PROJECT_SELECTION, {project})
// }

// imported 


export const getAllUserProjects = async ({state}) => {
  return await Api.getAllUserProjects(state.project.user.id, state.project.token)
}



export const selectProject = async ({commit, state}, {id}) => {
  await Api.updateUser(state.project.user.id, {
    currentProjectId: id
  }, state.project.token)
  let project = await Api.getProject(id, state.project.token)
  commit(types.PROJECT_SELECTION, {project})
}
