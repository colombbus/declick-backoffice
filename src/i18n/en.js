export default {
  "categories": {
    "home": "home",
    "board": "board",
    "users": "user",
    "teacher": "teacher",
    "circuit": "circuit",
    "circuits": "circuits",
    "documentation": "documentation",
    "writer": "writer",
    "groupes": "groupes",
  },
  "internals": {
    "about": "about",
    "news": "news",
    "state": "services state",
    "home": "accueil",
  },
  "menu": {
    "hide": "hide",
  },
};