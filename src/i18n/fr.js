export default {
  "categories": {
    "board": "tableau de bord",
    "users": "utilisateur",
    "teacher": "animateur",
    "circuit": "parcours",
    "circuits": "parcours",
    "documentation": "documentation",
    "writer": "participer",
    "groupes": "groupes",
  },
  "internals": {
    "about": "à propos",
    "news": "actualités",
    "state": "état des services",
    "home": "accueil",
  },
  "menu": {
    "hide": "masquer",
  },
};