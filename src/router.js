import Vue from "vue";
import Router from "vue-router";
// views
import Home from "./views/Home.vue";
import Writer from "./views/Writer.vue";
import Circuits from "./views/Circuits.vue";
import Groupes from "./views/Groupes.vue";
// import About from "./views/About.vue";

// #### components
import CreateCourse from "./components/circuits/courses/courseCreateNew.vue";
import GroupeUsers from "./components/groupes/GroupeUsers.vue";
import User from "./components/users/User.vue";

// #### imported from declick-ui
import UserList from "./components/users/UserList.vue";
import CircuitList from "./components/circuits/CircuitList.vue";
import CourseEditor from "./components/circuits/CourseEditor.vue";
import CourseList from "./components/circuits/courses/CourseList.vue";
import EditCourse from "./components/circuits/courses/EditCourse.vue";


import createView from "./components/circuits/create/CreateView.vue"

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    // categories
    {
      path: "/board",
      name: "board",
      type: "categories"
    },
    {
      path: "/users",
      name: "users",
      type: "categories",
      component: UserList
    },
    {
      path: '/user/:id',
      name: 'user',
      component: User
    },
    {
      path: "/teacher",
      name: "teacher",
      type: "categories",
    },
    {
      path: "/groupes",
      name: "groupes",
      type: "categories",
      component: Groupes,
    },
    {
      path: "/groupe/:id",
      name: "groupe users",
      component: GroupeUsers
    },
    {
      path: "/circuits",
      name: "circuits",
      type: "categories",
      component: Circuits,
      children: [
        {
          path: "list",
          name: "circuit list",
          type: "categories",
          component: CircuitList
        },
        {
          path: "list/:id",
          name: "circuit",
          type: "categories",
          component: CourseEditor
        },
        {
          path: "courses/list",
          name: "courses list",
          type: "categories",
          component: CourseList,
        },
        {
          path: "courses/edit/:id",
          name: "courses edit",
          type: "categories",
          component: EditCourse
        },
        {
          path: "courses/create",
          name: "courses create",
          type: "categories",
          component: createView
        }
      ]
    },
    {
      path: "/documentation",
      name: "documentation",
      type: "categories"
    },
    {
      path: "/writer",
      name: "writer",
      type: "categories",
      component: Writer
    },
    // internals
    {
      path: "/about",
      name: "about",
      type: "internals",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },

    {
      path: "/news",
      name: "news",
      type: "internals"
    },
    {
      path: "/state",
      name: "state",
      type: "internals"
    },
    {
      path: "/",
      name: "home",
      component: Home,
      type: "internals"
    }
  ]
});
